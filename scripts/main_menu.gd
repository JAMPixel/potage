extends Node2D

const music_scene = preload("res://nodes/music.tscn")

func _ready():
	var music = music_scene.instance()
	get_node("/root").add_child(music)
	music.play()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
