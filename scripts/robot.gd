extends PathFollow2D

const max_health = 200.0
const explosion_scene = preload("res://nodes/explosion.tscn")

var normal_speed = 20
var speed = normal_speed
var speed_factor = 1
var rewind_stack_size = 0
var health = 200.0
var path_length = 0 # automatically set by the robot factory
var tower_damage = 1

signal attack_tower

onready var plants = $"../../../../plants"
onready var health_bar = get_node("health_bar")
onready var body = $body
onready var animation_damage = $sprites/animation_damage
onready var rewind_animation = $rewind

signal killed

func _ready():
	rotate = false
	rotation = 0
	loop = false
	
	v_offset = rand_range(-20, 20)
	var _ign = connect("killed", self, "_on_killed")

func _process(delta):
	health_bar.value = health / max_health * 100
	var doffset = speed_factor * speed * delta
	if self.offset + doffset >= path_length:
		emit_signal("attack_tower", tower_damage)
		remove_reference_in_plants()
		queue_free()
	else:
		self.offset += doffset
		
	z_index = global_position.y as int

func remove_reference_in_plants():
	for plant in plants.get_children():
		plant.clean(body)
	
func _on_killed(pos):
	var explosion = explosion_scene.instance()
	get_node("/root/main").add_child(explosion)
	explosion.global_position = pos
	
func take_damage(damage):
	health -= damage
	animation_damage.play("damage")
	if health <= 0:
		emit_signal("killed", body.global_position)
		remove_reference_in_plants()
		queue_free()

func rewind(state):
	if state:
		rewind_stack_size += 1
		if rewind_stack_size == 1:
			speed_factor = -1.5
			rewind_animation.visible = true
	else:
		rewind_stack_size -= 1
		if rewind_stack_size == 0:
			speed_factor = 1
			rewind_animation.visible = false
