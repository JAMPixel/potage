extends Node2D

onready var animation_player = $animation_player

func _ready():
	animation_player.play("walk")
	$"firejet/animation_player".play("giggle")
	rotation = 0
