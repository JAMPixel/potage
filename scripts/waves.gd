extends Node

export(float, 0.0, 10.0) var first_wave_delay = 1.0
export(float, 1.0, 60.0) var next_wave_delay = 15.0
export(int, 1, 10) var spawn_per_wave = 3
export(float, 1.0, 15.0) var wave_spawn_duration = 5.0

var rng = RandomNumberGenerator.new()
var wave_timer = Timer.new()

signal spawn

func _ready():
	rng.randomize()
	
	var _b = get_tree().create_timer(first_wave_delay).connect("timeout", self, "wave")
	
	add_child(wave_timer)
	wave_timer.connect("timeout", self, "wave")
	wave_timer.one_shot = true
	wave_timer.start(next_wave_delay + first_wave_delay + wave_spawn_duration)

func wave():
	for _i in range(0, spawn_per_wave):
		var delay_before_spawn = rng.randf_range(0, wave_spawn_duration)
		var _a = get_tree().create_timer(delay_before_spawn).connect("timeout", self, "spawn")
	_increase_difficulty()
	print("next wave in "+str(next_wave_delay + wave_spawn_duration)+" s")
	print("will contain "+str(spawn_per_wave)+" bots")
	print("that will spawn in "+str(wave_spawn_duration))
	wave_timer.start(next_wave_delay + wave_spawn_duration)
	
func _increase_difficulty():
	var type_of_difficulty = rng.randi_range(0, 3)

	# 0 is for nothing
	if type_of_difficulty == 1: # increase robots per wave
		spawn_per_wave += 1
	elif type_of_difficulty == 2: # reduce the time the robots have to spawn
		wave_spawn_duration *= 0.95
	elif type_of_difficulty == 3:  # reduce the delay between waves
		next_wave_delay *= 0.95

func spawn():
	emit_signal("spawn")
