extends Node2D

onready var path = $path
onready var path_length = path.get_curve().get_baked_length()

const robot_scene = preload("res://nodes/robots/robot_a.tscn")

func spawn_robot():
	var robot = robot_scene.instance()
	path.add_child(robot)
	robot.path_length = path_length
	return robot
