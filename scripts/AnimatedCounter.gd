extends Label
class_name AnimatedCounter

export(int) var value = 0
export(float) var smooth_coef = .1

var real_value = 0.0


func _process(_delta):
	real_value = real_value * (1.0 - smooth_coef) + (value * smooth_coef)
	if abs(value - real_value) < 1.0:
		real_value = float(value)
	text = str(round(real_value))
