extends Node2D

func _on_replay_pressed():
	var _ignore = get_tree().change_scene("res://scenes/game.tscn")
	
func update_values(game):
	$"center/grid/score".value = game.score
	$"center/grid/seeds".value = game.seeds
	$"center/grid/cogs".value = game.cogs
	$"center/grid/nb_enemies".value = game.kill_count
