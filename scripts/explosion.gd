extends Node2D

onready var timer = $kill_timer

func _ready():
	z_index = (global_position.y + 1000) as int
	timer.connect("timeout", self, "queue_free")
	$particles.emitting = true
	get_node("/root/main/animation").play("giggle")
