tool
extends Node2D

onready var plant_wheel = $plant_wheel
onready var plant_wheel_click_area = $plant_wheel_click_area
onready var upgrade_wheel = $upgrade_wheel
onready var upgrade_wheel_clic_area = $upgrade_wheel_click_area
onready var upgrade_wheel_plant_area = $upgrade_wheel_click_area/plant_shape
onready var spawn_point = $spawn_point
onready var plant = null

func _ready():
	z_index = global_position.y as int
	
	plant_wheel_click_area.connect("clicked", self, "_on_plant_wheel")
	upgrade_wheel_clic_area.connect("clicked", self, "_on_upgrade_wheel")
	
	_update()

func _update():
	if is_instance_valid(plant):
		plant_wheel_click_area.visible = false
		upgrade_wheel_clic_area.visible = true
		
		# Possibly set value depending on plant
		upgrade_wheel.sell.set_value(10)
	else:
		plant_wheel_click_area.visible = true
		upgrade_wheel_clic_area.visible = false

func _on_plant_wheel():
	if not plant_wheel.visible:
		for pot in get_parent().get_children():
			pot.close()
	
	plant_wheel.toggle()
		
func _on_upgrade_wheel():
	if not upgrade_wheel.visible:
		for pot in get_parent().get_children():
			pot.close()
	
	upgrade_wheel.toggle()

func close():
	plant_wheel.close()
	upgrade_wheel.close()
	
func set_plant(p):
	plant = p
	_update()

func unset_plant():
	if is_instance_valid(plant):
		plant.queue_free()
	plant = null
	_update()

func upgraded(button):
	upgrade_wheel.upgraded(button)
