extends Control

onready var animation = $heart_group/heart/damage_animation
onready var label_heart = $heart_group/label_heart

func set_life(life, animate = true ):
	label_heart.text = str(life)
	if (animate):
		animation.play("damage_heart")
