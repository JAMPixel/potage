extends Node2D
class_name BasicShot

var basic_damage = 20

var target = null

signal make_damage

func _process(_delta):
	z_index = global_position.y as int
	
	if (target != null):
		var target_position = Vector2()
		if typeof(target) == TYPE_VECTOR2:
			target_position = target
		elif is_instance_valid(target):
			target_position = target.global_position
		else:
			pass
			
		var dist = Vector2(self.global_position).distance_to(target_position)
		if ( dist < 10 ):
			if (not typeof(target) == TYPE_VECTOR2): # Do not put damage if the robot is already dead
				emit_signal("make_damage", get_damage())
				on_hit(target)
			queue_free()
		else:
			self.global_position = self.global_position.move_toward(target_position, 5)
			self.global_rotation = target_position.angle_to_point(self.global_position)
			self.rotate(PI/2)

func on_hit(_enemy):
	pass

func get_damage():
	return basic_damage
