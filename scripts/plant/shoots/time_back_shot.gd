extends BasicShot

const speed_factor = -1.5

var rewind_duration = 2

func on_hit(enemy):
	if is_instance_valid(enemy):
		var e = enemy.get_parent() as PathFollow2D
		e.rewind(true)
		var _a = get_tree().create_timer(rewind_duration).connect("timeout", e, "rewind", [false])

func get_damage():
	return 1
