extends Plant

const fireplant_shot_scene = preload("res://nodes/plant_shots/basic_shot.tscn")

var damage = 20

func _ready():
	shot_timeout = 1

func get_shot_scene():
	return fireplant_shot_scene

func shot_configure(shot):
	shot.basic_damage = damage

func upgrade_special():
	damage *= 1.2
