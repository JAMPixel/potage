extends Plant

const time_back_shot_scene = preload("res://nodes/plant_shots/time_back_shot.tscn")

var rewind_duration = 1

func _ready():
	shot_timeout = 2.5

func get_shot_scene():
	return time_back_shot_scene

func shot_configure(shot):
	shot.rewind_duration = rewind_duration

func upgrade_special():
	rewind_duration *= 1.25
