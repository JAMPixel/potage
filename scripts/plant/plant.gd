extends Node2D
class_name Plant

const shot_scene = preload("res://nodes/plant_shots/basic_shot.tscn")

onready var area = $area
onready var circle = $area/circle
onready var shot_list = $shots
onready var shot_timeout = 1

var shots = Array().duplicate()
var can_shoot = true


func _process(_delta):
	try_to_shoot()

func get_shot_scene():
	return shot_scene
	
func get_shot_timeout():
	return shot_timeout

func try_to_shoot():
	var enemy = select_enemy_to_shoot()
	if (is_instance_valid(enemy) && can_shoot):
		shoot(enemy)

func shoot(enemy):
	var _a = get_tree().create_timer(get_shot_timeout()).connect("timeout", self, "reset_shot")
	can_shoot = false
	var shot = get_shot_scene().instance()
	shot_configure(shot)
	shot_list.add_child(shot)
	shot.set('target', enemy)
	shot.connect("make_damage", enemy.get_parent(), "take_damage")

func shot_configure(_shot):
	pass
	
func reset_shot():
	can_shoot = true

func select_enemy_to_shoot():
	var robot_list = area.get_overlapping_bodies()
	if (robot_list.empty()):
		 return null
	else:
		return robot_list[0];

func remove_target(robot):
	for shot in shot_list.get_children():
		if (not typeof(shot.get('target')) == TYPE_VECTOR2 && shot.get('target') == robot):
			shot.set('target', robot.global_position)

# Remove the reference to a robot
func clean(robot):
	remove_target(robot)

func upgrade_range():
	circle.shape.radius *= 1.1

func upgrade_rate():
	shot_timeout *= .95

func upgrade_special():
	pass

func upgrade(type):
	if type == "range":
		upgrade_range()
	elif type == "rate":
		upgrade_rate()
	elif type == "special":
		upgrade_special()
	else:
		print("bad type: "+type)

