extends Plant

const multi_shot_scene = preload("res://nodes/plant_shots/multi_shot.tscn")

func _ready():
	shot_timeout = 2

func get_shot_scene():
	return multi_shot_scene

func try_to_shoot():
	var enemy = select_enemy_to_shoot()
	if (is_instance_valid(enemy) && can_shoot):
		shoot_all(enemy)

func shoot_all(enemy):
	can_shoot = false
	for i in range(0, 10):
		var _a = get_tree().create_timer(i * .05).connect("timeout", self, "shoot", [enemy])
	var _a = get_tree().create_timer(get_shot_timeout()).connect("timeout", self, "reset_shot")

func shoot(enemy):
	if (is_instance_valid(enemy)):
		var enemy_parent = enemy.get_parent()
		var shot = get_shot_scene().instance()
		shot_list.add_child(shot)
		shot.set('target', enemy)
		if is_instance_valid(enemy_parent):
			shot.connect("make_damage", enemy_parent, "take_damage")

func upgrade_special():
	pass
