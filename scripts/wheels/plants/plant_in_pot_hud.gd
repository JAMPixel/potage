extends Sprite
class_name PlantInPot

onready var click_area = $click_area
onready var price_tag = $price_tag
onready var price_label = $price_tag/label
onready var sprite = $sprite

var plant_type = "unknown"
var price = 0

signal plant

func _ready():
	_set_gray_level(0);
	
	click_area.connect("clicked", self, "_clicked")
	
func _update():
	price_label.text = str(price)

func update_availability(seeds):
	_set_gray_level(seeds < price)
	click_area.visible = seeds >= price

func _clicked():
	emit_signal("plant")

func _set_gray_level(a):
	get_material().set_shader_param("a", a)
	sprite.get_material().set_shader_param("a", a)
	price_tag.get_material().set_shader_param("a", a)
