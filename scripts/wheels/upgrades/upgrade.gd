extends Sprite
class_name Upgrade

onready var click_area = $click_area
onready var price_tag = $price_tag
onready var price_label = $price_tag/label
onready var sprite = $sprite

var upgrade_type = "unknown"
var price = 0
var cogs = 0

signal upgrade

func _ready():
	_set_gray_level(0);

	click_area.connect("clicked", self, "_clicked")
	
func _update():
	price_label.text = str(price)
	update_availability(cogs)

func set_price(v):
	price = v
	_update()

func update_availability(available_cogs):
	cogs = available_cogs
	_set_gray_level(cogs < price)
	click_area.visible = cogs >= price

func _clicked():
	emit_signal("upgrade")

func _set_gray_level(a):
	get_material().set_shader_param("a", a)
	sprite.get_material().set_shader_param("a", a)
	price_tag.get_material().set_shader_param("a", a)
