extends Upgrade

func _ready():
	upgrade_type = "sell"
	price = 0
	
	_update()

func set_value(v):
	price = v
	_update()
	
# Selling is always available
func update_availability(_seeds):
	pass
