tool
extends Node2D
class_name Wheel

export(bool) var force_update_ui = false setget _force_update_ui

onready var buttons = $buttons
onready var circle = $circle
onready var scalex = circle.scale.x
onready var radius = circle.texture.get_width() * scalex / 2
onready var vector = Vector2(radius - 12*scalex, 0)
onready var animation_player = $animation_player

func _ready():
	animation_player.connect("animation_finished", self, "_hide")
	
	update_ui()

func update_ui():
	pass
	
func on_seeds_changed(_seeds):
	pass
	
func on_cogs_changed(_seeds):
	pass

func open():
	if not visible:
		visible = true
		animation_player.play("open")

func close():
	if visible:
		animation_player.play("close")

func toggle():
	if visible:
		close()
	else:
		open()

func _hide(anim_name):
	if anim_name == 'close':
		visible = false

func _force_update_ui(_arg):
	update_ui()
