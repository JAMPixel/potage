tool
extends Wheel

onready var sell = $buttons/sell

signal sell
signal upgrade

func update_ui():
	var count = buttons.get_child_count()
	var angle = -PI/2
	if count != 0:
		var angle_step = 2*PI/count
		for button in buttons.get_children():
			button.position = vector.rotated(angle)
			if not Engine.is_editor_hint():
				if button.upgrade_type == "sell":
					button.connect("upgrade", self, "_sell", [button])
				else:
					button.connect("upgrade", self, "_upgrade", [button])
			angle += angle_step

func on_cogs_changed(cogs):
	for button in buttons.get_children():
		button.update_availability(cogs)

func _sell(button):
	emit_signal("sell", self.get_parent(), button.price, 0)

func _upgrade(button):
	emit_signal("upgrade", self.get_parent(), button.upgrade_type, 0, button.price, button)

func upgraded(button):
	button.set_price(int(button.price * 1.2) + 1)
