tool
extends Wheel

signal plant

func update_ui():
	if is_instance_valid(buttons):
		var count = buttons.get_child_count()
		var angle = -PI/2
		if count != 0:
			var angle_step = 2*PI/count
			for button in buttons.get_children():
				button.position = vector.rotated(angle)
				if not Engine.is_editor_hint():
					button.connect("plant", self, "_plant", [button.plant_type, button.price, 0])
				angle += angle_step

func on_seeds_changed(seeds):
	for button in buttons.get_children():
		button.update_availability(seeds)

func _plant(plant_type, price_seeds, price_cogs):
	emit_signal("plant", self.get_parent(), plant_type, price_seeds, price_cogs)
