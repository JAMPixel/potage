extends Node2D

const spawn_delay = 4

export(int) var life = 5
export(int) var seeds = 200
export(int) var cogs = 100
export(int, 0, 1000) var robot_speed_min = 15
export(int, 0, 1000) var robot_speed_max = 30

var rng = RandomNumberGenerator.new()
var seed_provider_timer = Timer.new()
var elapsed_time = spawn_delay
var is_game_over = false
var kill_count = 0
var score = 0
var next_wave_time = 10.0
var spawn_per_wave = 3
var wave_spawn_duration = 3
var wave_timer = Timer.new()

onready var robots = $"elements/robots"
onready var hud = $hud
onready var plants = $plants
onready var pots = $elements/pots
onready var waves = $waves

const game_over_scene = preload("res://scenes/game_over.tscn")
const reward_popup_scene = preload("res://nodes/reward_popup.tscn")
const explosion_scene = preload("res://nodes/explosion.tscn")

const plant_scenes = {
	"fireplant": preload("res://nodes/plants/fireplant.tscn"),
	"pumpkin": preload("res://nodes/plants/pumpkin.tscn"),
	"plant_time_back": preload("res://nodes/plants/plant_time_back.tscn"),
}

signal life_changed
signal seeds_changed
signal cogs_changed
signal game_over

func _ready():
	rng.randomize()
	
	update_hud()
	hud.set_life(life, false)
	
	# Configure seed providing
	seed_provider_timer.connect("timeout", self, "provide_seed")
	seed_provider_timer.wait_time = 1
	add_child(seed_provider_timer)
	seed_provider_timer.start()
	
	var _a = connect("game_over", self, "on_game_over")
	
	for pot in pots.get_children():
		var _ignore
		_ignore = pot.plant_wheel.connect("plant", self, "on_plant")
		_ignore = pot.upgrade_wheel.connect("upgrade", self, "on_upgrade")
		_ignore = pot.upgrade_wheel.connect("sell", self, "on_sell")
		_ignore = connect("seeds_changed", pot.plant_wheel, "on_seeds_changed")
		_ignore = connect("cogs_changed", pot.plant_wheel, "on_cogs_changed")
		_ignore = connect("seeds_changed", pot.upgrade_wheel, "on_seeds_changed")
		_ignore = connect("cogs_changed", pot.upgrade_wheel, "on_cogs_changed")
	
	# Send information about the numbers of seeds, cogs, …
	spend(0, 0)
	
	waves.connect("spawn", self, "spawn")

func spawn():
	var robot = robots.spawn_robot()
	robot.connect("attack_tower", self, "on_tower_attacked")
	robot.connect("killed", self, "on_robot_killed")
	robot.speed = rand_range(robot_speed_min, robot_speed_max)

func provide_seed():
	spend_seeds(-1)
	
func alter_life(q):
	life += q
	hud.set_life(life)
	emit_signal("life_changed", life)

func spend_seeds(q):
	if q > seeds:
		return false
	
	seeds -= q
	$"hud/money_group/label_seeds".value = seeds
	emit_signal("seeds_changed", seeds)
	
	return true

func spend_cogs(q):
	if q > cogs:
		return false
	
	cogs -= q
	$"hud/money_group/label_cogs".value = cogs
	emit_signal("cogs_changed", cogs)
	
	return true

func spend(s, c):
	if s > seeds or c > cogs:
		return false
	
	seeds -= s
	cogs -= c
	$"hud/money_group/label_cogs".value = cogs
	$"hud/money_group/label_seeds".value = seeds
	emit_signal("seeds_changed", seeds)
	emit_signal("cogs_changed", cogs)
	
	return true
	
func update_hud():
	$"hud/money_group/label_cogs".value = cogs
	$"hud/money_group/label_seeds".value = seeds
	$"hud/score_group/label_score".value = score
		
func on_tower_attacked(damages):
	if is_game_over:
		return
	
	alter_life(-damages)
	if life <= 0:
		is_game_over = true
		for pot in pots.get_children():
			pot.close()
		emit_signal("game_over")
	score += 10000

func on_game_over():
	hud.visible = false

	var plant_list = plants.get_children()
	plant_list.shuffle()
	
	var time_to_wait_before_game_over = 0
	for plant in plant_list:
		var time = rng.randf_range(0, 1.5)
		time_to_wait_before_game_over = max(time_to_wait_before_game_over, time)
		var _a = get_tree().create_timer(time).connect("timeout", self, "explode_plant", [plant])
	
	var _a = get_tree().create_timer(time_to_wait_before_game_over + 0.4).connect("timeout", self, "display_game_over")
	
func explode_plant(plant):
	var explosion = explosion_scene.instance()
	get_node("/root/main").add_child(explosion)
	explosion.global_position = plant.global_position
	plant.queue_free()

func display_game_over():
	hud.visible = false
	var gm = game_over_scene.instance()
	add_child(gm)
	gm.update_values(self)

func on_plant(pot, plant_type, price_seeds, price_cogs):
	if not spend(price_seeds, price_cogs):
		return

	var plant = plant_scenes.get(plant_type).instance()
	plants.add_child(plant)
	pot.set_plant(plant)
	plant.global_transform = pot.spawn_point.global_transform
	plant.z_index = pot.z_index
	
	pot.close()
	
	incr_score(1000)

func on_upgrade(pot, upgrade_type, price_seeds, price_cogs, button):
	if not spend(price_seeds, price_cogs):
		return
	
	pot.plant.upgrade(upgrade_type)
	pot.close()
	pot.upgraded(button)
	
	pass

func on_sell(pot, value_seeds, value_cogs):
	spend(-value_seeds, -value_cogs)
	pot.unset_plant()
	pot.close()

func on_robot_killed(pos):
	kill_count += 1
	var popup = reward_popup_scene.instance()
	add_child(popup)
	popup.global_position = pos
	popup.label.text = "+3"
	spend_cogs(-3)
	incr_score(521)
	
func incr_score(value):
	score += value
	$"hud/score_group/label_score".value = score
